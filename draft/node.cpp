#pragma once

#include "node.h"

Node::Node(string name, std::vector<Node> initialNodes) noexcept{
    _name = name;
    _myNodes = initialNodes;
}

string Node::name(){
    return _name;
}

bool Node::containsGoal(string goal, std::vector<Node>& breadcrumbs){
    while(!_myNodes.empty()){
        if(Node::_myNodes[_myNodes.size()].containsGoal(goal, breadcrumbs) == true){
            breadcrumbs.push_back(*this);
            return true;
        }
        _myNodes.pop_back();
    }

    return false;
}