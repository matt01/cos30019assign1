#include<vector>
#include<string>
using std::string;

class Node{
    private:
        std::vector<Node> _myNodes;
        string _name;

    public:
        std::vector<Node> _myNodes;
        string _name;

        Node(string name, std::vector<Node> initialNodes) noexcept;

        string name();

        bool containsGoal(string goal, std::vector<Node>& breadcrumbs);

        std::vector<Node> getnodes();
};