#include<string>
using std::string;

class Route{
    private:
        string _destination;
        string _origin;
        int _actualCost;
        int _straightCost;

        int _cost;

    public:
        Route(string origin, string destination, int actualCost, int straightCost);

        string Destination();

        string Origin();

        int ActualCost();

        int Cost();

        string toString();
};