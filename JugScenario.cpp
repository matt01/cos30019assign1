#include<vector>
#include "Jugs.h"
#include "JugScenario.h"
#include "state.h"

bool JugScenario::isSolved(Jugs data){
    return data.getLeft() + data.getRight() == 1;
}

std::vector<Jugs> JugScenario::determineMoveSet(State<Jugs> state){
    std::vector<Jugs> moves;

    //current left/right values
    int left = state.Data().getLeft();
    int right = state.Data().getLeft();

    //can fill left jug?
    if(left < 3){
        moves.push_back(Jugs(3, right, "Filled Left"));
    }

    //can fill right jugs?
    if(right < 5){
        moves.push_back(Jugs(left, 5, "Filled Right"));
    }

    //can empty left jug?
    if(left > 0){
        moves.push_back(Jugs(0, right, "Emptied Left"));
    }

    //can empty right jug?
    if(right > 0){
        moves.push_back(Jugs(left, 0, "Emptied Right"));
    }

    if(left > 0 && right < 5){
        int tempLeft = left;
        int tempRight = right;

        while(tempLeft > 0 && tempRight > 5){
            tempRight++;
            tempLeft--;
        }

        moves.push_back(Jugs(tempLeft, tempRight, "Poured Left into Right"));
    }

    if(left < 3 && right > 0){
        int tempLeft = left;
        int tempRight = right;

        while(tempLeft < 3 && tempRight > 0){
            tempRight--;
            tempLeft++;
        }

        moves.push_back(Jugs(tempLeft, tempRight, "Poured Right into Left"));
    }
    return moves;
}