#include "Jugs.h"
#include<string>

Jugs::Jugs(int left, int right, std::string message){
    _left = left;
    _right = right;
    _message = message;
}

int Jugs::getLeft(){
    return _left;
}

int Jugs::getRight(){
    return _right;
}

int Jugs::getCost(){
    return _cost;
}

std::string Jugs::toString(){
    return _message;
}