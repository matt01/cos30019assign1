#include "route.h"

    Route::Route(string origin, string destination, int actualCost, int straightCost){
        _destination = destination;
        _origin = origin;
        _actualCost = actualCost;
        _straightCost = straightCost;

        _cost = 0;
    }

    string Route::Destination(){
        return _destination;
    }

    string Route::Origin(){
        return _origin;
    }

    int Route::ActualCost(){
        return _actualCost;
    }

    int Route::Cost(){
        return _cost;
    }

    string Route::toString(){
        return _origin;
    }