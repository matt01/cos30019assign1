#include<vector>
#include "route.h"
#include "Scenario.h"
#include "routeScenario.h"
using std::string;

    routeScenario::routeScenario(string condition, std::vector<Route> routes){
        _solvedCondition = condition;
        _routes = routes;
    }

    bool routeScenario::isSolved(Route data){
        return data.Origin() == _solvedCondition;
    }

    std::vector<Route> routeScenario::determineMoveSet(State<Route> state){
        std::vector<Route> moves;

         for(std::vector<Route>::iterator it = _routes.begin(); it != _routes.end(); ++it){
            //create state and add to queue if actual dist == -1
            if(((Route)state.Data()).Destination() == it->Origin() && it->ActualCost() != 1){
                moves.push_back(*it);
            }
        }
        return moves;
    }