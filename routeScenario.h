#include<vector>
#include<string>
#include "route.h"
#include "Scenario.h"
using std::string;

class routeScenario : public Scenario<Route>{
    private:
        std::vector<Route> _routes;
        std::string _solvedCondition;

    public:
        routeScenario(std::string condition, std::vector<Route> routes);

        bool isSolved(Route data);

        std::vector<Route> determineMoveSet(State<Route> state);
};