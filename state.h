#include<string>
using std::string;

template<typename T>
    class State{
        private:
            T _data;
            State _parent();
            string _message;
            int _cost;

        public:
            State(State parent, string message, T data){
                _parent = parent;
                _message = message;
                _data = data;
            }

            State(State parent, string message, T data, int cost){
                _parent = parent;
                _message = message;
                _data = data;
                _cost = cost;
            }

            State Parent(){
                return _parent;
            }

            T Data(){
                return _data;
            }

            string Message(){
                return _message;
            }

            int Cost(){
                return _cost;
            }
    };