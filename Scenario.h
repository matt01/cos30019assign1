#include<stack>
#include<vector>
#include "state.h"

template<typename T>
struct Scenario
{
    public:
        virtual std::vector<T> determineMoveSet(State<T> state);

        virtual bool isSolved(T data);

        void displaySolution(State<T> state, int searched, int discovered);
};