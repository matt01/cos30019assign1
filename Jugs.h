#include "StateData.h"
#include<string>

class Jugs : public StateData{
    private:
        int _left;
        int _right;

        std::string _message;
        int _cost;

    public:
        Jugs(int left, int right, std::string message);

        int getLeft();

        int getRight();

        int getCost();

        std::string toString();
};