#include<list>
#include "Scenario.h"

template<typename T>
class BFS{
    private:
        //queue
        std::list<State<T>> _frontier;

        Scenario<T> _scene;

        int _discovered; //num of discovered nodes
        int _searched; //num of discovered nodes that have been searched.

    public:
        BFS(Scenario<T> scene){
            _frontier = new std::vector<State<T>>();
            _scene = scene;
            _discovered = 0;
            _searched = 0;
        }

        void Search(State<T> initial){
            //if nodes already added with add, don't place first node in frontier to avoid doubling up
            if(initial.Data() != NULL){
                _frontier.push_back(initial);
            }
            State<T> state = NULL;

            while(!_frontier.empty()){
                state = _frontier.pop_front();

                _searched++;

                //is goal?
                if(_scene.isSolved(state.Data())){
                    break;
                }

                //add possible moves to the frontier
                addArrayToFrontier(_scene.determineMoveSet(state), state);
            }

            _scene.displaySolution(state, _searched, _discoverd);
        }

        void addArrayToFrontier(std::vector<T> data, State<T> parent){
            for(std::vector<T>::iterator it = data.begin(); it != data.end(); ++it){
                _frontier.push_back(new State<T>(parent, std::to_string(*it), *it));

                _discovered++;
            }
        }

        void addArrayToFrontier(std::vector<T> data, NULL){
            for(std::vector<T>::iterator it = data.begin(); it != data.end(); ++it){
                _frontier.push_back(new State<T>(NULL, std::to_string(*it), *it));

                _discovered++;
            }
        }
};