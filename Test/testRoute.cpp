#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<vector>
#include "/home/matt/Desktop/codestuff/cpp/COS30019/Tut4/route.h"
using std::cout;
using std::string;

std::vector<Route> readFile(){
    std::vector<Route> routes;
    string line;

    std::ifstream myfile("routes.txt");

    while(std::getline(myfile, line)){
        string words[4];
        int i = 0;

        std::istringstream iss(line);
        string word;
        while(std::getline(iss, word, ' ')){
            words[i] = word;
            i++;            
        }
        routes.push_back(Route(words[0], words[1], stoi(words[2]), stoi(words[3])));
    }

    return routes;
}

int main(){
    std::vector<Route> routes = readFile();

    for(std::vector<Route>::iterator it = routes.begin(); it != routes.end(); ++it){
        std::cout<<it->ActualCost()<<" ";
        std::cout<<it->Cost()<<" ";
        std::cout<<it->Origin()<<" ";
        std::cout<<it->Destination()<<" ";
        std::cout<<std::endl;
    }

    return 0;
}