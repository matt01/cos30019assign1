#include "Scenario.h"
#include "Jugs.h"
#include<vector>

class JugScenario : public Scenario<Jugs>{
    public:
        bool isSolved(Jugs data);

        std::vector<Jugs> determineMoveSet(State<Jugs> state);
};