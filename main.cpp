#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<vector>
#include "route.h"
#include "routeScenario.h"
#include "BFS.h"
#include "JugScenario.h"
using std::cout;
using std::string;

std::vector<Route> readFile(string filename){
    std::vector<Route> routes;
    string line;

    std::ifstream myfile(filename);

    while(std::getline(myfile, line)){
        string words[4];
        int i = 0;

        std::istringstream iss(line);
        string word;
        while(std::getline(iss, word, ' ')){
            words[i] = word;
            i++;            
        }
        routes.push_back(Route(words[0], words[1], stoi(words[2]), stoi(words[3])));
    }

    return routes;
}

int main(int argc, char *argv[]){
    string filename = argv[0];
    string origin = argv[1];
    string destination = argv[2];

    std::vector<Route> routes = readFile(filename);

    bool destFound = false;
    bool originFound = false;

    std::vector<Route> initialMoves;
    for(std::vector<Route>::iterator it = routes.begin(); it != routes.end(); ++it){
        if(it->Origin() == origin){
            initialMoves.push_back(*it);
            originFound = true;
        }
        if(!destFound && it->Origin() == destination){
            destFound = true;
        }
    }

    if(destFound && originFound){
        //Create new route scenario which has a search implementation specific for the search
        routeScenario rs(destination, routes);

        BFS<Route> bfs(rs);

        if(initialMoves.size() > 1){
            bfs.addArrayToFrontier(initialMoves, NULL);
            bfs.Search(State<Route>(NULL, origin, NULL));
        }
        else{
            bfs.Search(State<Route>(NULL, origin, *initialMoves.begin()));
        }
    }
    else{
        if(!destFound){
            std::cout<<destination<<" could not be found in the map file."<<std::endl;
        }
        if(!originFound){
            std::cout<<origin<<" could not be foudn in the maps file."<<std::endl;
        }
    }

    return 0;
}

void solveJugsProblem(){
        JugScenario jugs();
        BFS<Jugs> bfs(jugs);

        bfs.Search(State<Jugs>(NULL, "L:0 R:0 - Initial", Jugs(0, 0, "Initial")));
    }